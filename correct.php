<!doctype html>
    <html lang='en-GB'>
        <head>
            <title>Captcha Success</title>
            <link rel='stylesheet' href='css.css'>
        </head>
        <body>
            <div class='w3-content w3-card-8' style='max-width: 1080px; margin: auto;'>
                <header class='w3-container w3-teal w3-padding' style='margin-top: 2em;'>
                    <h4 class='w3-center w3-bolder'>Captcha Check</h4>        
                </header>
                <section class='w3-padding-16'>
                    <div class='w3-container'>
                        <div class='w3-panel w3-center w3-pale-greem' > 
                            <p>
                                You entered the correct code. Have a good time or <a href='index'>try again</a>.
                            </p>
                        </div>
                    </div>
                </section>
            </div>
        </body>
    </html>
    <?php
       unset($_SESSION['count']);
    ?>