<?php
    session_start();
    error_reporting(E_ALL & ~ E_NOTICE);
    $error = array();
    if ( isset($_POST['submit']) ){
        $text = trim($_POST['check']);
        if( $text !=$_SESSION['string'] ){
            array_push($error, 'push');
            header('location: wrong');
        } elseif($text === $_SESSION['string']){
            header('location: correct');
        }

       
    }
            //count invalid inputs
        if (!empty($error)){
            if ( !isset($_SESSION['count']) ){
                    $count = 1;
                    $_SESSION['count'] = $count;
            }elseif(isset($_SESSION['count']) ){
                $count = $_SESSION['count'];;
                $count++;
                $_SESSION['count'] = $count;
            }
        }
     //Lockout if moree than 5 wring tries
    if ($_SESSION['count'] >= 5){
      die('Locked out! too many incorrect inputs');
    }

?>
<!doctype html>
    <html lang='en-GB'>
        <head>
            <title>Captcha Check</title>
            <link rel='stylesheet' href='css.css'>
        </head>
        <body>
            <div class='w3-content w3-card-8' style='max-width: 1080px; margin: auto;'>
                <header class='w3-container w3-teal w3-padding' style='margin-top: 2em;'>
                    <h4 class='w3-center w3-bolder'>Captcha Check</h4>        
                </header>
                <section class='w3-padding-16'>
                    <div class='w3-container'>
                        <form action='' method='post'>
                            <p class='w3-text-teal'>Enter the text in the image below and click submit</p>
                            <div class='w3-row'>
                                <div class='w3-section'>
                                    <label class='w3-label w3-text-teal' for='check'>Enter the text in the image</label>    <img src='image' style='border: 0;'>   <input type='text' name='check' autocomplete='off' required>
                                </div>
                                <button type='submit' class='w3-button w3-teal' name='submit'>Submit</button>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </body>
    </html>